function validateLoginForm() {
  var emailVal = document.getElementById("email").value;
  var passwordVal = document.getElementById("password").value;

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  } else if (passwordVal == "") {
    alert("Please enter password");
    return false;
  } else if (passwordVal.length < 3) {
    alert("Password length must be atleast 3 characters");
    return false;
  } else {
    return true;
  }
}

function validateIsRegistredUser() {
  let users = getUsers();

  let emailVal = document.getElementById("email").value;
  var passwordVal = document.getElementById("password").value;

  let user = users.find((user) => {
    return user.email === emailVal && user.password === passwordVal;
  });

  if (user) {
    localStorage.setItem("loggedInUser", JSON.stringify(user));
    setLoginSession("userId", user.id, 30);
    return true;
  } else {
    alert("Wrong email or password. Please try again.");
    return false;
  }
}

function getLoginSession(cname) {
  if (document.cookie.length > 0) {
    cookie =
      document.cookie.match("(^|;)\\s*" + cname + "\\s*=\\s*([^;]+)")?.pop() ||
      "";
    return cookie;
  }
  return "";
}

function setLoginSession(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function checkLoginSession() {
  let userId = getLoginSession("userId");
  if (userId !== null && userId !== "") {
    return userId;
  } else {
    return null;
  }
}

function deleteCookie(name) {
  setLoginSession(name, null, -1);
}

function logOut() {
  deleteCookie("userId");
  localStorage.removeItem("loggedInUser");
  return true;
}

function register() {
  var fullName = document.getElementById("fullName").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var users = [];

  var user = {
    id: Number(new Date()),
    fullName: fullName,
    email: email,
    password: password,
  };

  if (!localStorage.getItem("users")) {
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
  } else {
    users = JSON.parse(localStorage.getItem("users"));
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
  }

  console.log(user);
  return true;
}

function validateRegistrationForm() {
  var fullName = document.getElementById("fullName").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var confirmPassword = document.getElementById("confirmPassword").value;

  var atPosition = email.indexOf("@");
  var dotPosition = email.lastIndexOf(".");

  if (fullName === "") {
    alert("Please enter your full name.");
    return false;
  } else if (email === "") {
    alert("Please enter your email.");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email.");
    return false;
  } else if (password === "") {
    alert("Please enter password.");
    return false;
  } else if (password.length < 3) {
    alert("Password length must be atleast 3 characters.");
    return false;
  } else if (confirmPassword === "") {
    alert("Please enter confirmation password.");
    return false;
  } else if (password !== confirmPassword) {
    alert("Password you entered didn't match.");
    return false;
  }
  return true;
}

//get users from the local storage:
function getUsers() {
  let usersRaw = localStorage.getItem("users");
  let users = [];
  if (!usersRaw) {
    users = [];
    return users;
  } else {
    users = JSON.parse(usersRaw);
    return users;
  }
}

//get the registred user from the local storage:
function getUser() {
  const userRaw = localStorage.getItem("loggedInUser");
  const user = JSON.parse(userRaw);
  return user;
}

//Validate edit form:
function validateUpdateForm() {
  var fullNameVal = document.getElementById("full-name-field").value;
  var emailVal = document.getElementById("email-field").value;

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (fullNameVal == "") {
    alert("Please enter full name");
    return false;
  } else if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  }

  return true;
}

//create uploads array of objects:
function createUploadFile() {
  let upload = {};
  let uploads = [];

  let uploadId = Date.now();
  let label = document.getElementById("labelInput");
  let user = JSON.parse(localStorage.getItem("loggedInUser"));

  upload.uploadId = uploadId;
  upload.label = label.value;
  upload.userId = user.id;
  upload.sharedTo = [];
  upload.fileName = "sample document";

  if (!localStorage.getItem("uploads")) {
    uploads.push(upload);
    localStorage.setItem("uploads", JSON.stringify(uploads));
    return true;
  } else {
    uploads = JSON.parse(localStorage.getItem("uploads"));
    uploads.push(upload);
    localStorage.setItem("uploads", JSON.stringify(uploads));
    return true;
  }
}

//get the uploads from the local storage:
function getUploads(filtered) {
  let uploadsRaw = localStorage.getItem("uploads");
  let uploads = JSON.parse(uploadsRaw);

  if (!uploads) {
    uploads = [];
  }

  if (filtered == 1) {
    var activeUser = getLoginSession("userId");
    var filteredUploads = uploads.filter(
      (upload) => upload.userId == activeUser
    );

    return filteredUploads;
  }

  return uploads;
}

//upload modal:
function uploadModal() {
  var modal = document.querySelector("#uploadModal");
  var span = document.querySelector("#closeUpload");
  var btnSave = document.querySelector("#save");
  var realBtn = document.querySelector("#realButton");
  var btnChooseFile = document.querySelector("#chooseFile");
  var label = document.querySelector("#labelInput");
  var fileName = document.querySelector("#fileName");
  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };

  btnChooseFile.onclick = function () {
    modal.style.display = "block";
    realBtn.click();
  };

  realBtn.onchange = function () {
    if (realBtn.value) {
      modal.style.display = "block";
      fileName.innerHTML = realBtn.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
    }
  };

  btnSave.onclick = function () {
    if (label.value !== "") {
      createUploadFile();
      modal.style.display = "none";
      location.reload();
      return true;
    } else {
      return false;
    }
  };
}

function displayUploadsTable() {
  var uploads = getUploads(1);
  for (let upload = 0; upload < uploads.length; upload++) {
    var uploadId = uploads[upload].uploadId;
    var label = uploads[upload].label;
    var fileName = "sample document";
    //uploads[upload].label

    const table = document
      .getElementById("uploadsTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newLabel = document.createTextNode(label);
    var newFilename = document.createTextNode(fileName);

    var actions = `<a class='noDeco' href='#' onclick='editUploadModal("${uploadId}"); return false;'>Edit</a> | <a class='noDeco' href='#' onclick='deleteUploadModal("${uploadId}"); return false;'>Delete</a> |  <a class='noDeco' href='./sharedPage.html?uploadId=${uploadId}&name=${fileName}'>Share</a>`;

    cell1.appendChild(newLabel);
    cell2.appendChild(newFilename);
    cell3.innerHTML += actions;
    cell2.className = "alignCenter";
    cell3.className = "alignCenter";
  }
}

//Edit Modal:
function updateUploadDetail(id, newLabel) {
  if (newLabel !== "") {
    let uploads = getUploads();

    for (let i = 0; i < uploads.length; i++) {
      if (uploads[i].uploadId == id) {
        uploads[i].label = newLabel;
        localStorage.setItem("uploads", JSON.stringify(uploads));

        return true;
      }
    }
  } else {
    alert("Please add a label for the file.");
    return false;
  }
}

function editUploadModal(id) {
  viewUploadDetail(id);
  var modal = document.querySelector("#editModal");
  var span = document.querySelector("#closeEdit");
  var btnUpdate = document.querySelector("#btnUpdate");
  var newLabel = document.querySelector("#editLabelInput");
  var cancelBtn = document.querySelector("#btn-edit-cancel");

  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };

  cancelBtn.onclick = function () {
    modal.style.display = "none";
  };

  btnUpdate.onclick = function () {
    modal.style.display = "none";
    if (newLabel.value !== "") {
      updateUploadDetail(id, newLabel.value);
    }
  };
}

function viewUploadDetail(id) {
  let uploads = getUploads();

  for (let upload = 0; upload < uploads.length; upload++) {
    if (uploads[upload].uploadId == id) {
      document.getElementById("editLabelInput").value = uploads[upload].label;
    }
  }
}

//for delete modal:
function deleteUploadModal(id) {
  var modalEl = document.querySelector("#deleteModal");
  var spanEl = document.querySelector("#closeDelete");
  var btnOk = document.querySelector("#btnOk");
  modalEl.style.display = "block";

  spanEl.onclick = function () {
    modalEl.style.display = "none";
  };
  btnOk.onclick = function () {
    modalEl.style.display = "none";
    deleteUpload(id);
  };
}

function deleteUpload(id) {
  var uploads = getUploads();
  for (let i = 0; i < uploads.length; i++) {
    if (uploads[i].uploadId == id) {
      uploads.splice(i, 1);
      localStorage.setItem("uploads", JSON.stringify(uploads));
    }
  }
}
function showRegisteredUsers() {
  let dropdown = document.querySelector("#usersDropdown");
  let users = getUsers();
  let activeUserId = getLoginSession("userId");

  for (let user = 0; user < users.length; user++) {
    if (users[user].id != activeUserId) {
      let option = document.createElement("option");
      option.value = users[user].id;
      option.innerHTML = users[user].fullName;
      dropdown.appendChild(option);
    }
  }
}

// ************** For shared page: ********************

function getUserInfo(id) {
  let users = getUsers();

  var user;
  for (i = 0; i < users.length; i++) {
    if (users[i].id == id) {
      user = users[i];
      break;
    } else {
      user = null;
    }
  }
  return user;
}

function populateSharedUsers(uploadId) {
  let uploads = JSON.parse(localStorage.getItem("uploads"));
  if (uploadId != null) {
    const upload = uploads.find((upload) => {
      return upload.uploadId == uploadId;
    });

    return upload.sharedTo;
  } else {
    return uploads;
  }
}

function displaySharedUsersTable(id) {
  let sharedUsers = populateSharedUsers(id);
  console.log("shared users: ", sharedUsers);

  for (user = 0; user < sharedUsers.length; user++) {
    var userId = sharedUsers[user];
    console.log(userId);
    var name = getUserInfo(userId);

    if (!name) {
      continue;
    }
    const table = document
      .querySelector("#sharedUserTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var newUser = document.createTextNode(name.fullName);

    var action = `<a class='noDeco' href='#' onclick='deleteSharedUserModal("${id}", " ${userId}"); return false;'>Remove</a>`;

    cell1.appendChild(newUser);
    cell2.innerHTML = action;
    cell2.className = "alignCenter";
  }
}

function populateSharedFiles() {
  var sharedFiles = [];
  var uploads = JSON.parse(localStorage.getItem("uploads"));
  if (!uploads) {
    uploads = [];
  }
  var activeUser = getLoginSession("userId");
  for (let i = 0; i < uploads.length; i++) {
    for (let j = 0; j < uploads[i].sharedTo.length; j++) {
      if (uploads[i].sharedTo[j] == activeUser) {
        sharedFiles.push(uploads[i]);
      }
    }
  }
  return sharedFiles;
}

function displaySharedFilesTable() {
  let sharedFiles = populateSharedFiles();

  for (file = 0; file < sharedFiles.length; file++) {
    var label = sharedFiles[file].label;
    var fileName = sharedFiles[file].fileName;
    var author = getUserInfo(sharedFiles[file].userId);
    if (!author) {
      continue;
    }
    const table = document
      .querySelector("#sharedUploadsTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newLabel = document.createTextNode(label);
    var newFileName = document.createTextNode(fileName);
    var newAuthor = document.createTextNode(author.fullName);

    cell1.appendChild(newLabel);
    cell2.appendChild(newFileName);
    cell2.className = "alignCenter";
    cell3.appendChild(newAuthor);
    cell3.className = "alignCenter";
  }
}

function shareFile(uploadId, selectedUser) {
  var uploads = getUploads();
  var upload = getUploads().find((upload) => {
    return upload.uploadId == uploadId;
  });

  var userShared = upload.sharedTo.find((userShared) => {
    return userShared == selectedUser;
  });

  if (!userShared) {
    upload.sharedTo.push(selectedUser);
    for (let i = 0; i < uploads.length; i++) {
      if (uploads[i].uploadId == upload.uploadId) {
        uploads[i] = upload;
        localStorage.setItem("uploads", JSON.stringify(uploads));
        return true;
      }
    }
  } else {
    alert("You already shared this file to this user.");
  }
}

// for chats page:

function formatDate(date) {
  let dayOfMonth = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();

  month = month < 10 ? "0" + month : month;
  dayOfMonth = dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth;
  hour = hour < 10 ? "0" + hour : hour;
  minutes = minutes < 10 ? "0" + minutes : minutes;

  return (
    year +
    "-" +
    month +
    "-" +
    dayOfMonth +
    " " +
    hour +
    ":" +
    minutes +
    ":" +
    seconds
  );
}

function sendMessage(message) {
  var userId = getLoginSession("userId");
  var today = new Date();
  var chats = [];
  var chat = {
    chatId: Date.now(),
    sender: userId,
    message: message,
    date: formatDate(today),
  };

  if (!populateChats()) {
    chats.push(chat);
    localStorage.setItem("chats", JSON.stringify(chats));
    return true;
  } else {
    chats = populateChats();
    chats.push(chat);
    localStorage.setItem("chats", JSON.stringify(chats));
    return true;
  }
}

function populateChats() {
  var chats = JSON.parse(localStorage.getItem("chats"));
  if (!chats) {
    return (chats = []);
  } else {
    return chats;
  }
}

function removeChats() {
  var element = document.querySelector("#messages");
  element.innerHTML = "";
}

function displayAllChats() {
  let chats = populateChats();
  const messages = document.querySelector("#messages");

  for (let i = 0; i < chats.length; i++) {
    let messageBody = chats[i].message;
    let dateSent = chats[i].date;
    let messageElement = document.createElement("p");
    let sender = chats[i].sender;
    if (getUserInfo(sender) == null) {
      messageElement.innerHTML += `[${dateSent}] Deleted User: ${messageBody}`;
      messageElement.className = "msg-el";
      messages.appendChild(messageElement);
    } else if (sender == getLoginSession("userId")) {
      messageElement.innerHTML += `[${dateSent}] You : ${messageBody}`;
      messageElement.className = "msg-el";
      messages.appendChild(messageElement);
    } else {
      const name = getUserInfo(sender).fullName;
      messageElement.innerHTML += `[${dateSent}] ${name} : ${messageBody}`;
      messageElement.className = "msg-el";
      messages.appendChild(messageElement);
    }
  }
}

function deleteSharedUserModal(uploadId, targetUser) {
  var modal = document.querySelector("#deleteSharedUserModal");
  var span = document.querySelector("#closeDeleteSharedUser");
  var btnConfirm = document.querySelector("#btnDeleteSharedUser");
  var btnCancel = document.querySelector("#btnCancelDeleteSharedUser");
  modal.style.display = "block";

  btnConfirm.onclick = function () {
    deleteSharedUser(uploadId, targetUser);
    modal.style.display = "none";
    location.reload();
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  span.onclick = function () {
    modal.style.display = "none";
  };
}

function deleteSharedUser(uploadId, targetUser) {
  const user = targetUser.replace(/ /g, "");
  let uploads = getUploads();
  if (uploadId !== null) {
    var upload = getUploads().find((upload) => {
      return upload.uploadId == uploadId;
    });

    for (let i = 0; i < upload.sharedTo.length; i++) {
      if (upload.sharedTo[i] == user) {
        upload.sharedTo.splice(i, 1);
      }
    }

    for (let i = 0; i < uploads.length; i++) {
      if (uploads[i].uploadId == upload.uploadId) {
        uploads[i] = upload;
        localStorage.setItem("uploads", JSON.stringify(uploads));
      }
    }
  } else {
    for (let i = 0; i < uploads.length; i++) {
      for (let j = 0; j < uploads[i].sharedTo.length; j++) {
        if (uploads[i].sharedTo[j] == user) {
          uploads[i].sharedTo.splice(j, 1);
        }
      }
    }
    localStorage.setItem("uploads", JSON.stringify(uploads));
  }
}
